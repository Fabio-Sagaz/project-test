from tkinter import*
from tkinter import messagebox
from tkinter import ttk
#from PIL import Image, ImageTk
import DataBaser
import sqlite3

def sair(ResultA):
    ResultA.destroy()
    return

def ResultAluno(aluno):
    ResultA = Toplevel()
    ResultA.title('RESULTADO DO ALUNO')
    ResultA.geometry('750x300')
    ResultA.resizable(False,False)
    ResultA.attributes('-alpha', 0.9)
#======================FRAMES=====================================================
    LEFTFrame = Frame(ResultA, width=500, height=750, bg='#2F4F4F', relief=GROOVE)
    LEFTFrame.pack(side=LEFT)

    RIGHTFrame = Frame(ResultA, width=245, height=750, bg='#2F4F4F', relief=GROOVE)
    RIGHTFrame.pack(side=RIGHT)
#===================================================================================

#=================================IMAGE============================================
    PhotoRPV = PhotoImage(file=('icons/ALRPV.png'))
    PhotoRPVLabel = Label(LEFTFrame, image=PhotoRPV, bg='#2F4F4F')
    PhotoRPVLabel.place(x=1000)

    PhotoPV = PhotoImage(file=('icons/ALPV.png'))
    PhotoPVLabel = Label(LEFTFrame, image=PhotoPV, bg='#2F4F4F')
    PhotoPVLabel.place(x=1000)

    PhotoBRA = PhotoImage(file=('icons/BRANOTA.png'))
    PhotoBRALabel = Label(RIGHTFrame, image=PhotoBRA, bg='#2F4F4F')
    PhotoBRALabel.place(x=20,y=109)

    PhotoRPV = PhotoImage(file=('icons/ALRPV.png'))
    PhotoRPVLabel = Label(LEFTFrame, image=PhotoRPV, bg='#2F4F4F')
    PhotoRPVLabel.place(x=1000)

    PhotoAPROVA = PhotoImage(file=('icons/CALU.png'))
    PhotoAPROVALabel = Label(LEFTFrame, image=PhotoAPROVA, bg='#2F4F4F')
    PhotoAPROVALabel.place(x=1000)

    PhotoREPRO = PhotoImage(file=('icons/ALURPV.png'))
    PhotoREPROLabel = Label(LEFTFrame, image=PhotoREPRO, bg='#2F4F4F')
    PhotoREPROLabel.place(x=1000)
#=====================================================================
    Lin0 = Label(LEFTFrame, text=100*'=', bg='#2F4F4F')
    Lin0.place(x=0,y=20)

    Lin1 = Label(LEFTFrame, text=100*'=', bg='#2F4F4F')
    Lin1.place(x=0,y=90)  

    NomeCLabel = Label(LEFTFrame, text='NomeCompleto:{}'.format(aluno[1]), font=('Centyrt Gothic',15), fg='black', bg='#2F4F4F', relief=GROOVE)
    NomeCLabel.place(x=30,y=125)
    CPFLabel = Label(LEFTFrame, text='CPF:{}'.format(aluno[3]), font=('Centyrt Gothic',15), fg='black', bg='#2F4F4F', relief=GROOVE)
    CPFLabel.place(x=30,y=157)
    UsuarLabel = Label(LEFTFrame, text='Genero:{}'.format(aluno[2]), font=('Centyrt Gothic',15), fg='black', bg='#2F4F4F', relief=GROOVE)
    UsuarLabel.place(x=30,y=188)
    UsuarLabel = Label(LEFTFrame, text='Nota:{}'.format(aluno[7]), font=('Centyrt Gothic',15), fg='black', bg='#2F4F4F', relief=GROOVE)
    UsuarLabel.place(x=30,y=220)   

    Lin2 = Label(LEFTFrame, text=100*'=', bg='#2F4F4F')
    Lin2.place(x=0,y=248)  

    Lin3 = Label(RIGHTFrame, text=100*'=', bg='#2F4F4F')
    Lin3.place(x=0,y=20)
    NotaAluno = Label(RIGHTFrame, text='{}'.format(aluno[7]), font=('Century Gothic',35), bg='white', fg='black')
    NotaAluno.place(x=70, y=135)
    NotaText = Label(RIGHTFrame, text='NOTA DO ALUNO', font=('Century Gothic',20), bg='#2F4F4F', fg='black')
    NotaText.place(x=10, y=50)

    Lin3 = Label(RIGHTFrame, text=100*'=', bg='#2F4F4F')
    Lin3.place(x=0,y=90) 

    if(aluno[7] >= 7 ):

        PhotoRPVLabel.place(x=1000) #Sair do seu Local
        PhotoPVLabel.place(x=370,y=37)#Volta para seu Local
        PhotoAPROVALabel.place(x=400,y=100)#Volta para seu Local
        TextAprovLabel = Label(LEFTFrame, text='ALUNO - APROVADO', font=('Centyrt Gothic',20), fg='green', bg='#2F4F4F')
        TextAprovLabel.place(x=100,y=50)
        NotaAluno = Label(RIGHTFrame, text='{}'.format(aluno[7]), font=('Century Gothic',35), fg='green',bg='white')
        NotaAluno.place(x=70, y=135)
    elif(aluno[7] <= 6):
        PhotoRPVLabel.place(x=390,y=37) #Voltando
        PhotoREPROLabel.place(x=400,y=100) #Voltando
        TextAprovLabel = Label(LEFTFrame, text='ALUNO - REPROVADO', font=('Centyrt Gothic',20), fg='red', bg='#2F4F4F')
        TextAprovLabel.place(x=100,y=50)
        NotaAluno = Label(RIGHTFrame, text='{}'.format(aluno[7]), font=('Century Gothic',35), fg='red',bg='white')
        NotaAluno.place(x=70, y=135)
      
    textSair = Label(LEFTFrame, text='Sair da Sessão de Resultado', font=('Century Gothic',13),fg='black', bg='#2F4F4F')
    textSair.place(x=160, y=270)

    buttonSair = ttk.Button(LEFTFrame, text='Sair', width=7, command=lambda:sair(ResultA))
    buttonSair.place(x=405, y=270)

    ResultA.mainloop()
    return
def consultar(aluno,ConAlun):
    conn = sqlite3.connect('UsersData.db')
    DataBaser.cursor = conn.cursor()
    DataBaser.cursor.execute('SELECT * FROM UsersAluno')

    for row in DataBaser.cursor.fetchall():
        if row[0] == ConAlun.get():
            aluno = row
            ResultAluno(aluno)
            DataBaser.conn.commit()
            return
        
    messagebox.showerror(title='ERROR NO ID', message='Id de Aluno Não Encontrado')

       
   
    return
def SairdaVeri(VeriProf):
    VeriProf.destroy()
    return

def verProf(prof):
    VeriProf = Toplevel()
    VeriProf.title('INFORMAÇÃO DO CADASTRO')
    VeriProf.geometry('600x400')
    VeriProf.resizable(False,False)
    VeriProf.attributes('-alpha', 0.9)

    
    LEFTFrame = Frame(VeriProf, width=270, height=400, bg='MIDNIGHTBLUE', relief='raise')
    LEFTFrame.pack(side=LEFT)

    RIGHTFrame = Frame(VeriProf, width=325, height=400, bg='#2F4F4F', relief='raise')
    RIGHTFrame.pack(side=RIGHT)
    
    PhotoICF = PhotoImage(file='icons/ICF.png')
    PhotoLabeICF = Label(LEFTFrame, image=PhotoICF, bg='white')
    PhotoLabeICF.place(x=0)


    Lin1 = Label(RIGHTFrame, text=100*'=', bg='#2F4F4F')
    Lin1.place(x=0,y=40)

    Suainf = Label(RIGHTFrame,text='SUAS INFORMAÇÕES', font=('Century Gothic',15),fg='black', bg='#2F4F4F')
    Suainf.place(x=70, y=60)

    Lin2 = Label(RIGHTFrame, text=100*'=', bg='#2F4F4F')
    Lin2.place(x=0,y=90)

    NomeCLabel = Label(RIGHTFrame, text='NomeCompleto:{}'.format(prof[1]), font=('Centyrt Gothic',13), fg='black', bg='white', relief=GROOVE)
    NomeCLabel.place(x=20,y=120)
    CPFLabel = Label(RIGHTFrame, text='CPF:{}'.format(prof[3]), font=('Centyrt Gothic',13), fg='black', bg='white', relief=GROOVE)
    CPFLabel.place(x=20,y=145)
    UsuarLabel = Label(RIGHTFrame, text='Genero:{}'.format(prof[2]), font=('Centyrt Gothic',13), fg='black', bg='white', relief=GROOVE)
    UsuarLabel.place(x=20,y=170)
    UsuarLabel = Label(RIGHTFrame, text='Usuario:{}'.format(prof[4]), font=('Centyrt Gothic',13), fg='black', bg='white', relief=GROOVE)
    UsuarLabel.place(x=20,y=195)
    UsuarLabel = Label(RIGHTFrame, text='Cargo:{}'.format(prof[6]), font=('Centyrt Gothic',13), fg='black', bg='white', relief=GROOVE)
    UsuarLabel.place(x=20,y=220)
 
    Lin3 = Label(RIGHTFrame, text=100*'=', bg='#2F4F4F')
    Lin3.place(x=0,y=260)

    Seguinf = Label(RIGHTFrame, text='VOCÊ ESTÁ EM UM AMBIENTE PROTEGIDO!✅',font=('Century Gothic',10), fg='black', bg='#2F4F4F')
    Seguinf.place(x=20, y=286)

    Lin4 = Label(RIGHTFrame, text=100*'=', bg='#2F4F4F')
    Lin4.place(x=0,y=310)

    SairVeri = Label(RIGHTFrame, text='Sair da Verificação!', font=('Century Gothic',10), fg='black', bg='#2F4F4F')
    SairVeri.place(x=100,y=330)

    SairButton = ttk.Button(RIGHTFrame, text='Sair', width=5, command=lambda:SairdaVeri(VeriProf))
    SairButton.place(x=230,y=330)
    
    AmbienteS = Label(RIGHTFrame, text='Sua Informação estão,Protegida!✅', font=('Century Gothic',10), bg='#2F4F4F', fg='white',relief=GROOVE)
    AmbienteS.place(x=60, y=360)

    Programador = Label(RIGHTFrame, text='Sem Riscos de Vazamento de dados✅', font=('Century Gothic',10), bg='#2F4F4F',fg='white',relief=GROOVE)
    Programador.place(x=60,y=380)


    VeriProf.mainloop()
    return


def SairProf(Prof):
    Prof.destroy()
    return
def PortaldoProfessor(prof,aluno):
    Prof = Toplevel()
    Prof.title("PORTAL DO PROFESSOR")
    Prof.geometry("600x450")
    Prof.resizable(width=False, height=False)
    Prof.attributes('-alpha',0.9)
    
    TOPFrame = Frame(Prof, width=600, height=150, bg='white', relief='raise')
    TOPFrame.pack(side=TOP)

    LEFTFrame = Frame(Prof, width=600, height=310, bg='#2F4F4F', relief='raise')
    LEFTFrame.pack(side=LEFT)
    
    PhotoPRFS = PhotoImage(file='icons/PRF.png')
    PhotoLabelPRF = Label(TOPFrame, image=PhotoPRFS, bg='white')
    PhotoLabelPRF.place(x=0)
    
    PhotoALS = PhotoImage(file='icons/ALS.png')
    PhotoALSLabel = Label(LEFTFrame, image=PhotoALS, bg='#2F4F4F')
    PhotoALSLabel.place(x=5, y=180)

    LabelProfText = Label(LEFTFrame, text='Seja Bem-Vindo Professor:{}'.format(prof[1]), font=('Century Gothic',15), bg='#2F4F4F', fg='black', relief=GROOVE)
    LabelProfText.place(x=70, y=30)

    LabelCText = Label(LEFTFrame, text='Verificar Seu Cadastro?', font=('Century Gothic',11), bg='#2F4F4F', fg='white', relief=FLAT)
    LabelCText.place(x=70,y=65)

    ButtonC = ttk.Button(LEFTFrame, text='Verificar', width=8, command=lambda:verProf(prof))
    ButtonC.place( x=250, y=65)

    Lin1 = Label(LEFTFrame, text=100*'=', fg='black' , bg='#2F4F4F')
    Lin1.place(x=0, y=90)

    LabelAlunoCon = Label(LEFTFrame, text='CONSULTA DE ALUNOS', font=('Century Gothic',20), bg='#2F4F4F', fg='black')
    LabelAlunoCon.place(x=150, y=110)

    Lin2 = Label(LEFTFrame, text=100*'=', fg='black', bg='#2F4F4F')
    Lin2.place(x=0,y=160)
    
    AlunNomeText = Label(LEFTFrame, text='Informe Id do Aluno!', font=('Century Gothic',17), fg='black', bg='#2F4F4F')
    AlunNomeText.place(x=250, y=180)

    ConAlun = IntVar()
    ConAlunEntry = ttk.Entry(LEFTFrame, textvariable=ConAlun, width=20)
    ConAlunEntry.place(x=250,y=222)

    ConAlunButton = ttk.Button(LEFTFrame, text='Consultar', width=9, command=lambda:consultar(aluno,ConAlun))
    ConAlunButton.place(x=380,y=220)

    
    textSairProf = Label(LEFTFrame, text='Sair da Area do Professor!', font=('Century Gothic',13), fg='black', bg='#2F4F4F', relief=GROOVE)
    textSairProf.place(x=310, y=270)

    buttonSairProf = ttk.Button(LEFTFrame, text='Sair', width=8, command=lambda:SairProf(Prof))
    buttonSairProf.place(x=530, y=271)
    

    Prof.mainloop()
    return


def ConfirmarNota(aluno,nota):

    print('c',nota)
    print('id',aluno[0])
    conn = sqlite3.connect('UsersData.db')
    cursor = conn.cursor()
    cursor.execute("""
                UPDATE UsersAluno
                SET NomeCompleto = ?, Genero = ?,NumCPF = ?, Usuario = ?, Cargo = ?, Nota = ?
                WHERE id = ?
        """, (aluno[1],aluno[2],aluno[3],aluno[4],aluno[6],nota,aluno[0]))
    conn.commit()
    conn.close()
    
    messagebox.showinfo(title='REGISTRADO', message='Nota Armazenada no Banco de Dados !✅')
    print('nota:',nota)
    return

def Saiir(Apresent_Resp):
        Apresent_Resp.destroy() 
        return
def Minimizar(Apresent_Resp):
        Apresent_Resp.geometry('600x150')
        return

def VerResult(Apresent_Resp):
        Apresent_Resp.geometry('600x500')
        return

def Conferir(Q1, Q2, Q3, PegQ4,PegQ5,PegQ6,Q_7,Q_8,Q_9,Q_10, aluno):
    Apresent_Resp = Toplevel()
    Apresent_Resp.title('RESULTADO')
    Apresent_Resp.configure(bg='#2F4F4F')
    Apresent_Resp.geometry('600x150')
    Apresent_Resp.attributes('-alpha', 0.9)
    Apresent_Resp.resizable(width=False, height=False)
      

    


    TOPFrame = Frame(Apresent_Resp, width=600, height=150, bg='MIDNIGHTBLUE', relief='raise')
    TOPFrame.pack(side=TOP)

    LEFTFrame = Frame(Apresent_Resp, width=310, height=310, bg='#2F4F4F', relief='raise')
    LEFTFrame.pack(side=LEFT)

    PhotoRobo = PhotoImage(file='icons/Robo.png')
    RobLabel = Label(Apresent_Resp, image=PhotoRobo, bg='#2F4F4F')
    RobLabel.place(x=299, y=160)

    RIGHTFrame = Frame(Apresent_Resp, width=170, height=297, bg='white',relief='raise')
    RIGHTFrame.pack(side=RIGHT)

    #==============Loading Image=============================
    PhotoB = PhotoImage(file='icons/BRA.png')
    PhotoR = PhotoImage(file='icons/FR.png')
    PhotoRobAP = PhotoImage(file='icons/RobAP.png')
    PhotoRoboRP = PhotoImage(file='icons/RoboRP.png')
   
    #=========================================================
    
    nota = 0

    if( Q1.get() == '3 minutos' or Q1.get() == '3 minutos'):
        Label(RIGHTFrame, text='1º:{}✅'.format(Q1.get()), font=('Century Gothic',12),fg='Green', bg='white', relief=GROOVE).place(x=30, y=50)
        nota+= 1
        print('1°  ',Q1.get())
    else: 
        Label(RIGHTFrame, text='1º:{}❌'.format(Q1.get()), font=('Century Gothic',12),fg='red', bg='white', relief=GROOVE).place(x=30, y=50)
        nota+= 0
        print('1°  ',Q1.get())
    if( Q2.get()== '6 velas' or Q2.get == '6 Velas' or  Q2.get == '6Velas'):
        Label(RIGHTFrame, text='2º:{}✅'.format(Q2.get()), font=('Century Gothic',12), fg='Green', bg='white', relief=GROOVE).place(x=30,y=75)
        nota+= 1
        print('2°  ',Q2.get())
    else:
        Label(RIGHTFrame, text='2º:{}❌'.format(Q2.get()), font=('Century Gothic',12), fg='red', bg='white', relief=GROOVE).place(x=30,y=75)
        nota+= 0
        print('2°  ',Q2.get())
    if( Q3.get()== '13:45' or Q3.get == '01:45'):
        Label(RIGHTFrame, text='3º:{}✅'.format(Q3.get()), font=('Century Gothic',12), fg='Green', bg='white', relief=GROOVE).place(x=30,y=100)
        nota+= 1
        print('3°  ',Q3.get())
    else:
        Label(RIGHTFrame, text='3º:{}❌'.format(Q3.get()), font=('Century Gothic',12), fg='red', bg='white', relief=GROOVE).place(x=30,y=100)
        nota+= 0
        print('3°  ',Q3.get())
    if( PegQ4.get()== 18):
        Label(RIGHTFrame, text='4º:{}✅'.format(PegQ4.get()), font=('Century Gothic',12), fg='Green', bg='white', relief=GROOVE).place(x=30,y=125)
        nota+= 1
        print('4°  ',PegQ4.get())
    else:
        Label(RIGHTFrame, text='4º:{}❌'.format(PegQ4.get()), font=('Century Gothic',12), fg='red', bg='white', relief=GROOVE).place(x=30,y=125)
        nota+= 0
        print('4°  ',PegQ4.get())
    if( PegQ5.get()== 132):
        Label(RIGHTFrame, text='5º:{}✅'.format(PegQ5.get()), font=('Century Gothic',12), fg='Green', bg='white', relief=GROOVE).place(x=30,y=150)
        nota+= 1
        print('5°  ',PegQ5.get())
    else:
        Label(RIGHTFrame, text='5º:{}❌'.format(PegQ5.get()), font=('Century Gothic',12), fg='red', bg='white', relief=GROOVE).place(x=30,y=150)
        nota+= 0
        print('5°  ',PegQ5.get())
    if( PegQ6.get()== 12):
        Label(RIGHTFrame, text='6º:{}✅'.format(PegQ6.get()), font=('Century Gothic',12), fg='Green', bg='white', relief=GROOVE).place(x=30,y=175)
        nota+= 1
        print('6°  ',PegQ6.get())
    else:
        Label(RIGHTFrame, text='6º:{}❌'.format(PegQ6.get()), font=('Century Gothic',12), fg='red', bg='white', relief=GROOVE).place(x=30,y=175)
        nota+= 0
        print('6°  ',PegQ6.get())
    if( Q_7.get() == '14450'):
        Label(RIGHTFrame, text='7º:{}✅'.format(Q_7.get()), font=('Century Gothic',12), fg='Green', bg='white', relief=GROOVE).place(x=30,y=200)
        nota+= 1
        print('7°  ',Q_7.get())
    else:
        Label(RIGHTFrame, text='7º:{}❌'.format(Q_7.get()), font=('Century Gothic',12), fg='red', bg='white', relief=GROOVE).place(x=30,y=200)
        nota+= 0
        print('7°  ',Q_7.get())
    if( Q_8.get() == '8'):
        Label(RIGHTFrame, text='8º:{}✅'.format(Q_8.get()), font=('Century Gothic',12), fg='Green', bg='white', relief=GROOVE).place(x=30,y=225)
        nota+= 0
        print('8°  ',Q_8.get())
    else:
        Label(RIGHTFrame, text='8º:{}❌'.format(Q_8.get()), font=('Century Gothic',12), fg='red', bg='white', relief=GROOVE).place(x=30,y=225)
        nota+= 0
        print('8°  ',Q_8.get())
    if( Q_9.get() == '486'):
        Label(RIGHTFrame, text='9º:{}✅'.format(Q_9.get()), font=('Century Gothic',12), fg='Green', bg='white', relief=GROOVE).place(x=30,y=247)
        nota+= 1
        print('9°  ',Q_9.get())
    else:
        Label(RIGHTFrame, text='9º:{}❌'.format(Q_9.get()), font=('Century Gothic',12), fg='red', bg='white', relief=GROOVE).place(x=30,y=247)
        nota+= 0
        print('9°  ',Q_9.get())
    if( Q_10.get() == '3000'):
        Label(RIGHTFrame, text='10º:{}✅'.format(Q_10.get()), font=('Century Gothic',12), fg='Green', bg='white', relief=GROOVE).place(x=30,y=270)
        nota+=1
        print('10°  ',Q_10.get())
    else:
        Label(RIGHTFrame, text='10º:{}❌'.format(Q_10.get()), font=('Century Gothic',12), fg='red', bg='white', relief=GROOVE).place(x=30,y=270)
        nota+= 0
        print('10°  ',Q_10.get())
    
    print(nota) 
   
    

    LabelPhotoA = Label(TOPFrame, image=PhotoB, bg='MIDNIGHTBLUE')
    LabelPhotoA.place(x=420,y=30)
    LabelPhotoR = Label(TOPFrame, image=PhotoR, bg='MIDNIGHTBLUE')
    LabelPhotoR.place(x=0,y=0)
    NomeCLabel = Label(TOPFrame, text='NomeCompleto:{}'.format(aluno[1]), font=('Centyrt Gothic',12), fg='white', bg='MIDNIGHTBLUE', relief=GROOVE)
    NomeCLabel.place(x=160,y=5)
    CPFLabel = Label(TOPFrame, text='CPF:{}'.format(aluno[3]), font=('Centyrt Gothic',12), fg='white', bg='MIDNIGHTBLUE', relief=GROOVE)
    CPFLabel.place(x=160,y=30)
    UsuarLabel = Label(TOPFrame, text='Genero:{}'.format(aluno[2]), font=('Centyrt Gothic',12), fg='white', bg='MIDNIGHTBLUE', relief=GROOVE)
    UsuarLabel.place(x=470,y=5)
    UsuarLabel = Label(TOPFrame, text='Usuario:{}'.format(aluno[4]), font=('Centyrt Gothic',12), fg='white', bg='MIDNIGHTBLUE', relief=GROOVE)
    UsuarLabel.place(x=160,y=55)
    UsuarLabel = Label(TOPFrame, text='Cargo:{}'.format(aluno[6]), font=('Centyrt Gothic',12), fg='white', bg='MIDNIGHTBLUE', relief=GROOVE)
    UsuarLabel.place(x=470,y=30)
 

    Perguntatxt = Label(TOPFrame, text='{},Veja Seu Resultado na Prova?'.format(aluno[4]), font=('Century Gothic',10), fg='white', bg='MIDNIGHTBLUE')
    Perguntatxt.place(x=160, y=97)
    PerguntButton = ttk.Button(TOPFrame, text='VER!', width=7, command=lambda:VerResult(Apresent_Resp))
    PerguntButton.place(x=447, y=97)
    SeuResultxt = Label(RIGHTFrame,  text ='Seu Resultado!', font=('Century Gothic',10), fg='black', bg='white', relief=GROOVE)
    SeuResultxt.place(x=40, y=10)
    Linha = Label(RIGHTFrame, text=40*'=', fg='black', bg='white')
    Linha.place(x=0, y=30)


    if( nota == 7 or nota > 7):
        PhoRobAP = Label(LEFTFrame, image=PhotoRobAP, bg='#2F4F4F')
        PhoRobAP.place(x=20, y=-3)
         
    elif( nota == 6 or nota < 6):
        PhoRobAP = Label(LEFTFrame, image=PhotoRoboRP, bg='#2F4F4F')
        PhoRobAP.place(x=20, y=-3)
       
    Minimi = ttk.Button(Apresent_Resp,text='MINIMIZAR', width=15, command=lambda:Minimizar(Apresent_Resp))
    Minimi.place(x=330, y=473)

    Sair = ttk.Button(Apresent_Resp, text='SAIR',width=10, command=lambda:Saiir(Apresent_Resp))
    Sair.place(x=260, y=473)

    ok = ttk.Button(Apresent_Resp, text='CONFIRMA', width=12, command=lambda:ConfirmarNota(aluno,nota))
    ok.place(x=178, y=473)
    

    Apresent_Resp.mainloop()
    return


def Desitir (Questio):
        Questio.destroy()
        messagebox.showinfo(title='ALUNO' , message='Você Optou, Por Desistir da Prova!')
        return

def ProvadoAluno(aluno):
    Questio = Toplevel()
    Questio.geometry("1050x700")
    Questio.maxsize(1050, 750)
    Questio.title("QUESTIONARIO DO ALUNO")
    Questio.configure(bg='#2F4F4F')
    Questio.attributes('-alpha',0.9)
    Questio.resizable(width=False, height=False)

    #==========================Frames=========================================================
    TopFrame = Frame(Questio, width=1050, height=150,bg='MIDNIGHTBLUE', relief='raise')
    TopFrame.pack(side=TOP)

    PergunFrame = Frame(Questio, width=515, height=530, bg='MIDNIGHTBLUE', relief='raise')
    PergunFrame.pack(side=LEFT)
        
    MarcaFrame = Frame(Questio, width=515, height=530, bg='MIDNIGHTBLUE', relief='raise')
    MarcaFrame.pack(side=RIGHT)

    #==================================IMAGE=========================================================       
    PVPhoto = PhotoImage(file='icons/FT.png')
    GRPhoto = PhotoImage(file='icons/GR.png')
    VLSPhoto = PhotoImage(file='icons/VL.png')
    RLGPhoto = PhotoImage(file='icons/RLG.png')
        
    GRLabel = Label(PergunFrame, image=GRPhoto, bg='MIDNIGHTBLUE')
    GRLabel.place(x=0, y=100)
        
    PVLabel = Label(TopFrame, image=PVPhoto, bg='MIDNIGHTBLUE')
    PVLabel.place(x=0, y=0)
        
    VLSLabel = Label(PergunFrame, image=VLSPhoto, bg='MIDNIGHTBLUE')
    VLSLabel.place(x=0, y=250)
    RLGLabel = Label(PergunFrame, image=RLGPhoto, bg='MIDNIGHTBLUE')
    RLGLabel.place(x=0,y=350)

    #=======================================PERGUNTAS=======================================================
    
    Q1 = StringVar()
    Q2 = StringVar()
    Q3 = StringVar()

    LabelM = Label(PergunFrame, text='CHARADAS MATEMÁTICAS', font=('Century Gothic',20), fg='Black', bg='MIDNIGHTBLUE', relief=GROOVE)
    LabelM.place(x=45, y=5)

    PrimQ = Label(PergunFrame, text='1ª) Se 3 gatos matam 3 ratos em 3 minutos, quanto tempo levarão 100 gatos',font=('Century Gothic',10), fg='white',bg='MIDNIGHTBLUE')
    PrimQ.place(x=0, y=60)
    PrimCQ = Label(PergunFrame, text='para matar 100 ratos?, OBS:Responda,Adicionado Strings: ?minutos\n\t\t\t\tRESPONDA:',font=('Century Gothic',10), fg='white',bg='MIDNIGHTBLUE',justify=LEFT)
    PrimCQ.place(x=1, y=80)

    PrimQEntry =ttk.Entry(PergunFrame,textvariable=Q1, width=20)
    PrimQEntry.place(x=307, y=103)

    SegunQ = Label(PergunFrame, text='2ª)Em uma igreja havia 4 velas. Entraram 2 ladrões e cada um levou uma vela.\n Quantas velas ficaram? OBS:Responda,Adicionado Strings: ?velas\n\t\t\t\tRESPONDA:',font=('Century Gothic',10), fg='white',bg='MIDNIGHTBLUE',justify=LEFT)
    SegunQ.place(x=0, y=200)
    SeguQEnty=ttk.Entry(PergunFrame,textvariable=Q2, width=20)
    SeguQEnty.place(x=307, y=239)
        
    TercQ = Label(PergunFrame, text='3ª)Uma mãe tem 30 reais para dividir entre duas filhas. Qual o horário?\n OBS:Responda,Adicionado Dois Pontos: ?:? \n\t\t\t\tRESPONDA:',font=('Century Gothic',10), fg='white',bg='MIDNIGHTBLUE', justify=LEFT)
    TercQ.place(x=0, y=360)

    TercQEntry = ttk.Entry(PergunFrame,textvariable=Q3, width=20)
    TercQEntry.place(x=307, y=397)
 #============================================PERGUNTAS DE MARCA===========================================================================   
    LabelM2 = Label(MarcaFrame, text='MATEMATÍCA BÁCISA', font=('Century Gothic',20), fg='black', bg='MIDNIGHTBLUE', relief=GROOVE)
    LabelM2.place(x=110,y=5)
        
    MarcaQ4 =Label(MarcaFrame, text="4ª)Leandro tem 40 balas chupou 12 e deu 10 para sua irmã.Com Quantas\nbalas ele ficou?\t\tRESPONDA ALTERNATIVA CORRETA! ", font=('Century Gothic',10), fg='white', bg='MIDNIGHTBLUE', justify=LEFT)
    MarcaQ4.place(x=0, y=47)
    PegQ4 = IntVar()
    A_Marca4 = Radiobutton(MarcaFrame, text='A) 15', variable=PegQ4, value=15, font=('Century Gothic',13),fg='Black',bg='MIDNIGHTBLUE')
    A_Marca4.place(x=100, y=83) 
    B_Marca4 = Radiobutton(MarcaFrame, text='B) 18', variable=PegQ4, value=18, font=('Century Gothic',13),fg='Black',bg='MIDNIGHTBLUE')
    B_Marca4.place(x=184, y=83)
    C_Marca4 = Radiobutton(MarcaFrame, text='C) 14', variable=PegQ4, value=14, font=('Century Gothic',13),fg='Black',bg='MIDNIGHTBLUE')
    C_Marca4.place(x=266, y=83)
    D_Marca4 = Radiobutton(MarcaFrame, text='D) 16', variable=PegQ4, value=16, font=('Century Gothic',13),fg='Black',bg='MIDNIGHTBLUE')
    D_Marca4.place(x=350, y=83)
    E_Marca4 = Radiobutton(MarcaFrame, text='E) 13', variable=PegQ4, value=13, font=('Century Gothic',13),fg='Black',bg='MIDNIGHTBLUE')
    E_Marca4.place(x=428, y=83)

    MarcaQ5 =Label(MarcaFrame, text="5ª)Quantos números 1 eu tenho de 1 até 191?RESPONDA ALTERNATIVA CORRETA! ", font=('Century Gothic',10), fg='white', bg='MIDNIGHTBLUE', justify=LEFT)
    MarcaQ5.place(x=0, y=120)
    PegQ5 = IntVar()
    A_Marca5 = Radiobutton(MarcaFrame, text='A) 140', variable=PegQ5, value=140, font=('Century Gothic',13),fg='Black',bg='MIDNIGHTBLUE')
    A_Marca5.place(x=100, y=140) 
    B_Marca5= Radiobutton(MarcaFrame,  text='B) 136', variable=PegQ5, value=136, font=('Century Gothic',13),fg='Black',bg='MIDNIGHTBLUE')
    B_Marca5.place(x=184, y=140)
    C_Marca5 = Radiobutton(MarcaFrame, text='C) 132', variable=PegQ5, value=132, font=('Century Gothic',13),fg='Black',bg='MIDNIGHTBLUE')
    C_Marca5.place(x=266, y=140)
    D_Marca5 = Radiobutton(MarcaFrame, text='D) 150', variable=PegQ5, value=150, font=('Century Gothic',13),fg='Black',bg='MIDNIGHTBLUE')
    D_Marca5.place(x=350, y=140)
    E_Marca5 = Radiobutton(MarcaFrame, text='E) 126', variable=PegQ5, value=126, font=('Century Gothic',13),fg='Black',bg='MIDNIGHTBLUE')
    E_Marca5.place(x=428, y=140)

    MarcaQ6 =Label(MarcaFrame, text="6ª)Pedro tem 30 anos e tem mais três irmãos: Bianca de 27, Victor de 23 e Alex\nde 18. Qual a diferença de idade entre Pedro e irmão caçula?", font=('Century Gothic',10), fg='white', bg='MIDNIGHTBLUE', justify=LEFT)
    MarcaQ6.place(x=0, y=180)
    PegQ6 = IntVar()
    A_Marca6 = Radiobutton(MarcaFrame, text='A) 13', variable=PegQ6, value=13 , font=('Century Gothic',13),fg='Black',bg='MIDNIGHTBLUE')
    A_Marca6.place(x=100, y=220) 
    B_Marca6 = Radiobutton(MarcaFrame, text='B) 11', variable=PegQ6, value=11, font=('Century Gothic',13),fg='Black',bg='MIDNIGHTBLUE')
    B_Marca6.place(x=184, y=220)
    C_Marca6 = Radiobutton(MarcaFrame, text='C) 12', variable=PegQ6, value=12, font=('Century Gothic',13),fg='Black',bg='MIDNIGHTBLUE')
    C_Marca6.place(x=266, y=220)
    D_Marca6 = Radiobutton(MarcaFrame, text='D) 14', variable=PegQ6, value=14, font=('Century Gothic',13),fg='Black',bg='MIDNIGHTBLUE')
    D_Marca6.place(x=350, y=220)
    E_Marca6 = Radiobutton(MarcaFrame, text='E) 16', variable=PegQ6, value=16, font=('Century Gothic',13),fg='Black',bg='MIDNIGHTBLUE')
    E_Marca6.place(x=428, y=220)

#=====================================================PERGUNTAS===================================================================
        
    Q_7 = StringVar()
    Q_8 = StringVar()
    Q_9 = StringVar()
    Q_10 = StringVar()
        
    MarcaQ7 =Label(MarcaFrame, text="7ª)Eu tenho R$ 15000,00 gastei R$ 450,00. Quanto sobrou ?", font=('Century Gothic',10), fg='white', bg='MIDNIGHTBLUE', justify=LEFT)
    MarcaQ7.place(x=0, y=250)
    ObsQ7 = Label(MarcaFrame, text='(Somente o número, sem vírgula ou moeda)', font=('Century Gothic',10), fg='black', bg='MIDNIGHTBLUE',relief=GROOVE)
    ObsQ7.place(x=210,y=276)
    MarcaQ7Entry = Entry(MarcaFrame,textvariable=Q_7,)
    MarcaQ7Entry.place(x=15, y=278)

    MarcaQ8 =Label(MarcaFrame, text="8ª)Quanto é o resultado dessa conta 6+2x(4-3)?", font=('Century Gothic',10), fg='white', bg='MIDNIGHTBLUE', justify=LEFT)
    MarcaQ8.place(x=0, y=300)
    MarcaQ8Entry = Entry(MarcaFrame,textvariable=Q_8, )
    MarcaQ8Entry.place(x=15, y=330)

    MarcaQ9 =Label(MarcaFrame, text="9ª)Quanto é 9²x6?", font=('Century Gothic',10), fg='white', bg='MIDNIGHTBLUE', justify=LEFT)
    MarcaQ9.place(x=0, y=360)
    MarcaQ9Entry = Entry(MarcaFrame,textvariable=Q_9,)
    MarcaQ9Entry.place(x=15, y=390)
        
    MarcaQ10 =Label(MarcaFrame, text="10ª)Quanto é o dobro 1500?", font=('Century Gothic',10), fg='white', bg='MIDNIGHTBLUE', justify=LEFT)
    MarcaQ10.place(x=0, y=420)
    MarcaQ10Entry = Entry(MarcaFrame,textvariable=Q_10,)
    MarcaQ10Entry.place(x=15, y=445)

    Lin = Label(MarcaFrame, text=80*"=", bg='MIDNIGHTBLUE', fg=('black'), relief=FLAT)
    Lin.place(x=0, y=467)
    Finalizatxt = Label(MarcaFrame, text='Deseja Finalizar a Prova?', font=('Century Gothic',10), fg='black', bg='MIDNIGHTBLUE', relief=GROOVE)
    Finalizatxt.place(x=20,y=490)
    Finaliza = ttk.Button(MarcaFrame, text='Finalizar', width=10, command=lambda:Conferir(Q1, Q2, Q3, PegQ4,PegQ5,PegQ6,Q_7,Q_8,Q_9,Q_10, aluno))
    Finaliza.place(x=190, y=490)
    
    Desitirtxt = Label(MarcaFrame, text='Desitir da Prova?', font=('Century Gothic',10), fg='black', bg='MIDNIGHTBLUE', relief=GROOVE)
    Desitirtxt.place(x=280, y=492)
    DesitirButton = ttk.Button(MarcaFrame, text='Desitir', width=10, command=lambda:Desitir(Questio))
    DesitirButton.place(x=400, y=490)
    

    Questio.mainloop()
    return



def RegisterToDataBaser(NomeCompleEntry,NumCPFEntry,GeneroEntry,UsuarioEntry,SenhaEntry,AouP):
        NomeCompleto = NomeCompleEntry.get()
        NumCPF = NumCPFEntry.get()
        Genero = GeneroEntry.get()
        Usuario = UsuarioEntry.get()
        Senha = SenhaEntry.get()
        Cargo = AouP.get()
    
        if(NomeCompleto == '' and NumCPF=='' and Genero =='' and Usuario == '' and Senha == '' and Cargo ==''):
            messagebox.showerror(title='ERROR NO REGISTRO', message='Preenchar Todos os Dados!') 
        elif(Genero==Genero and NomeCompleto == '' and NumCPF=='' and Usuario == '' and Senha == '' and Cargo =='' ):
                messagebox.showerror(title='ERROR NO REGISTRO', message='Preenchar Completamente, os Dados!')
        elif(NomeCompleto == NomeCompleto and NumCPF=='' and Usuario == '' and Senha == '' and Cargo =='' ):
                 messagebox.showerror(title='ERROR NO REGISTRO', message='Preenchar Completamente, os Dados!')   
        elif(NumCPF==NumCPF and NomeCompleto == '' and Usuario == '' and Senha == '' and Cargo =='' ):
                 messagebox.showerror(title='ERROR NO REGISTRO', message='Preenchar Completamente, os Dados!')
        elif(Usuario == Usuario and NumCPF=='' and NomeCompleto == '' and Senha == '' and Cargo =='' ):
                 messagebox.showerror(title='ERROR NO REGISTRO', message='Preenchar Completamente, os Dados!')
        elif(Senha == Senha and Usuario == '' and NumCPF=='' and NomeCompleto == ''  and Cargo =='' ):
                 messagebox.showerror(title='ERROR NO REGISTRO', message='Preenchar Completamente, os Dados!')
        elif(Cargo == Cargo and Senha == '' and Usuario == '' and NumCPF=='' and NomeCompleto == ''):
                 messagebox.showerror(title='ERROR NO REGISTRO', message='Preenchar Completamente, os Dados!')
        elif(NomeCompleto == '' and NumCPF==NumCPF and Genero ==Genero and Usuario == Usuario and Senha == Senha and Cargo ==Cargo):    
                 messagebox.showerror(title='ERROR NO REGISTRO', message='Informe Seu Nome Completo!')         
        elif(NumCPF=='' and NomeCompleto==NomeCompleto and Genero ==Genero and Usuario == Usuario and Senha == Senha and Cargo ==Cargo):
                  messagebox.showerror(title='ERROR NO REGISTRO', message='Informe Seu CPF!')
        elif(Genero == '' and NomeCompleto==NomeCompleto and NumCPF == NumCPF and Usuario == Usuario and Senha == Senha and Cargo ==Cargo):
                  messagebox.showerror(title='ERROR NO REGISTRO', message='Informe Seu Genero!')          
        elif(Usuario =='' and NomeCompleto==NomeCompleto and NumCPF == NumCPF and Usuario == Usuario and Senha == Senha and Cargo ==Cargo):
                  messagebox.showerror(title='ERROR NO REGISTRO', message='Informe Seu Usuario!')
        elif(Senha=='' and NomeCompleto==NomeCompleto and Genero ==Genero and Usuario == Usuario and NumCPF == NumCPF and Cargo ==Cargo):
                  messagebox.showerror(title='ERROR NO REGISTRO', message='Informe Sua Senha!')
        elif(Cargo=='' and NomeCompleto==NomeCompleto and Genero ==Genero and Usuario == Usuario and Senha == Senha):
                  messagebox.showerror(title='ERROR NO REGISTRO', message='Informe seu Cargo!')
        elif(NomeCompleto == NomeCompleto and NumCPF==NumCPF and Usuario == Usuario and Senha == Senha and Cargo == 'Aluno'):
            if(Genero == 'Masculino' or Genero == 'masculino' or Genero == 'feminino' or Genero =='Feminino'):
                     DataBaser.cursor.execute("""
                     INSERT INTO UsersAluno(NomeCompleto, NumCPF, Genero, Usuario, Senha, Cargo,Nota) VALUES(?, ?, ?, ?, ?, ?,'')
                     """,(NomeCompleto, NumCPF, Genero, Usuario, Senha, Cargo))
                     DataBaser.conn.commit()
                     messagebox.showinfo(title='ALUNO - REGISTRADO', message='Registrando Com Sucesso!✅')
            else:
                     messagebox.showerror(title='ERROR NO REGISTRO', message='Genero Invalido!')
        elif(NomeCompleto == NomeCompleto and NumCPF==NumCPF and Usuario == Usuario and Senha == Senha and Cargo == 'Professor'):
            if(Genero == 'Masculino' or Genero == 'masculino' or Genero == 'feminino' or Genero =='Feminino'):
                     DataBaser.cursor.execute("""
                     INSERT INTO UsersProf(NomeCompleto, NumCPF, Genero, Usuario, Senha, Cargo) VALUES(?, ?, ?, ?, ?, ?)
                     """,(NomeCompleto, NumCPF, Genero, Usuario, Senha, Cargo))
                     DataBaser.conn.commit()
                     messagebox.showinfo(title='PROFESSOR - REGISTRADO', message='Registrando Com Sucesso!✅')
            else:
                     messagebox.showerror(title='ERROR NO REGISTRO', message='Genero Invalido!')             


def Entrar(Usuario,Senha,Decissao):
    Usuario = Usuario.get()
    Senha = Senha.get()
    Cargo = Decissao.get()

    conn = sqlite3.connect('UsersData.db')
    DataBaser.cursor = conn.cursor()

    if(Cargo == 'Professor'):
            DataBaser.cursor.execute("""
            SELECT * FROM UsersProf
            WHERE (Usuario = ? and Senha = ? and Cargo = ?)
            """, (Usuario, Senha, Cargo))
            print('Acessando Professor')
            VeriAcesso = DataBaser.cursor.fetchone()
            if(Usuario in VeriAcesso and Senha in VeriAcesso and  Cargo in VeriAcesso):
                print("ENTRAR") 
                pegar(Senha, Usuario, Cargo) 
            else:
                messagebox.showerror(title='ENTRADA NEGADA', message='Usuario ou Senha; Cargo Estão Incorreto!')  
    elif(Cargo == 'Aluno'):
            DataBaser.cursor.execute("""
            SELECT * FROM UsersAluno
            WHERE (Usuario = ? and Senha = ? and Cargo = ?)
            """, (Usuario, Senha, Cargo))
            print('Acessando Aluno')
            VeriAcesso = DataBaser.cursor.fetchone()
            if(Usuario in VeriAcesso and Senha in VeriAcesso and  Cargo in VeriAcesso):  
                print("ENTRAR")   
                pegar(Senha, Usuario, Cargo)
            else:
                   messagebox.showerror(title='ENTRADA NEGADA', message='Usuario ou Senha; Cargo Estão Incorreto!')              
    elif(Usuario == Usuario and Senha == Senha and Cargo == Cargo):
        messagebox.showerror(title='ENTRADA NEGADA', message='Usuario ou Senha; Cargo Estão Incorreto!')
    return
    


def Fecha(Reg):
    Reg.destroy()
    return

def Reset(NomeCompleEntry,NumCPFEntry,GeneroEntry,UsuarioEntry,SenhaEntry):
        NomeCompleEntry.delete(0,END)
        NumCPFEntry.delete(0,END)
        GeneroEntry.delete(0,END)
        UsuarioEntry.delete(0,END)
        SenhaEntry.delete(0,END)
        return


def Criar():
    Reg = Toplevel()
    Reg.title("REGISTRAR NO SISTEMA")
    Reg.configure(bg='white')
    Reg.geometry('560x450')
    Reg.resizable(width=False, height=False)
    Reg.attributes("-alpha", 0.9)
    Reg.iconbitmap(default='icons/fatepi.ico')

    TopFrame = Frame(Reg, width= 600, height=150, bg='MIDNIGHTBLUE', relief='raise')
    TopFrame.pack(side=TOP)

    bottomFrame = Frame(Reg, width=600, height=295, bg='MIDNIGHTBLUE', relief='raise')
    bottomFrame.pack(side = BOTTOM)

    #=============LORDING IMG================#
    REGPhoto = PhotoImage(file='icons/REG.png')
    DSLabel = Label(TopFrame, image=REGPhoto)
    DSLabel.place(x=0,y=0)
    #========================================#
 
    NomeComple = Label(bottomFrame, text='Nome Completo:', font=('Century Gothic',18), fg='white',bg='MIDNIGHTBLUE')
    NomeComple.place(x=25, y=19)
    NomeC = StringVar()
    NomeCompleEntry = ttk.Entry(bottomFrame, width=30, textvariable=NomeC)
    NomeCompleEntry.place(x=240,y=30)

    
    NumCPF = Label(bottomFrame, text='CPF:' , font=('Century Gothic',18), fg='white', bg='MIDNIGHTBLUE')
    NumCPF.place(x=175,y=50)
    CpF = StringVar()
    NumCPFEntry = Entry(bottomFrame, width=30, textvariable=CpF)
    NumCPFEntry.place(x=240, y=63)

    Genero = Label(bottomFrame, text='Genero:', font=('Century Gothic',18), fg='white', bg='MIDNIGHTBLUE')
    Genero.place(x=130, y=80)
    Gen = StringVar()
    GeneroEntry = Entry(bottomFrame, width=30, textvariable=Gen)
    GeneroEntry.place(x=240, y=90)
    
    UsuarioLabel = Label(bottomFrame, text='Usuario:', font=('Century Gothic', 20), bg='MIDNIGHTBLUE', fg='white')
    UsuarioLabel.place(x=110,y=110)
    Usuar = StringVar()
    UsuarioEntry = ttk.Entry(bottomFrame,width=30,textvariable=Usuar)
    UsuarioEntry.place(x=219, y=122,)

    SenhaLabel = Label(bottomFrame, text='Senha:', font=('Century Gothic', 20), bg='MIDNIGHTBLUE', fg='white')
    SenhaLabel.place(x=125,y=150)
    Senha = StringVar()
    SenhaEntry = ttk.Entry(bottomFrame, width=30,textvariable=Senha, show='•')
    SenhaEntry.place(x=219, y=161)

    CargoLabel = Label(bottomFrame, text='Cargo:', font=('Century Gothic',20), bg='MIDNIGHTBLUE', fg='white')
    CargoLabel.place(x=125,y=188)

    AouP = StringVar()
    Prof = Radiobutton(bottomFrame, text='Professor', font=('Century Gothic',15), value='Professor',  variable=AouP, bg='MIDNIGHTBLUE',fg='black')
    Prof.place(x=330, y=195)

    Alun = Radiobutton(bottomFrame, text='Aluno',font=('Century Gothic',15), value='Aluno', variable=AouP, bg='MIDNIGHTBLUE',fg='black')
    Alun.place(x=225, y=195)

    Apaga = ttk.Button(bottomFrame, text='LIMPAR', width=10, command=lambda:Reset(NomeCompleEntry,NumCPFEntry,GeneroEntry,UsuarioEntry,SenhaEntry))
    Apaga.place(x=210, y=240)
    
    RegistraC = ttk.Button(bottomFrame, text='REGISTRA', width=15, command=lambda:RegisterToDataBaser(NomeCompleEntry,NumCPFEntry,GeneroEntry,UsuarioEntry,SenhaEntry,AouP))
    RegistraC.place(x=299, y=240)

    FechaButton = ttk.Button(bottomFrame, text='FECHAR', width=10, command=lambda:Fecha(Reg))
    FechaButton.place(x=130, y=240)

    Reg.mainloop()

def Exit(Jan):
    Jan.destroy()
    return

def pegar(senha, user,Cargo):    
    conn = sqlite3.connect('UsersData.db')
    DataBaser.cursor.execute("select * from UsersAluno")

    if (Cargo == 'Aluno'):
        print("Aluno")
        for row in DataBaser.cursor.fetchall():
            if senha == row[5] and user == row[4]:
                aluno = row
                ProvadoAluno(aluno)
                conn.commit()
                conn.close()
                return 0
    elif (Cargo == 'Professor'):
         conn = sqlite3.connect('UsersData.db')
         DataBaser.cursor.execute("select * from UsersAluno")
         for rowA in DataBaser.cursor.fetchall():
            aluno = rowA
         conn = sqlite3.connect('UsersData.db')
         DataBaser.cursor.execute("select * from UsersProf")
         for row in DataBaser.cursor.fetchall():
            if senha == row[5] and user == row[4]:
                prof = row
                PortaldoProfessor(prof,aluno)
                conn.commit()
                conn.close()
                return 0
    conn.commit()
    conn.close()


def Menu():
    Jan = Tk()
    Jan.title("ENTRAR NO SISTEMA")
    Jan.configure(bg='white')
    Jan.geometry('560x450')
    Jan.resizable(width=False, height=False)
    Jan.attributes("-alpha", 0.9)
    Jan.iconbitmap(default='icons/fatepi.ico')

    #==========================CARREGANDO IMG===================================================
    ESPhoto = PhotoImage(file='icons/ES.png')
    BVPhoto = PhotoImage(file='icons/BV.png')
    RobPhoto = PhotoImage(file='icons/Rob.png')

    #============================================================================================
    TopFrame = Frame(Jan, width= 600, height=150, bg='MIDNIGHTBLUE', relief='raise')
    TopFrame.pack(side=TOP)

    bottomFrame = Frame(Jan, width=600, height=295, bg='MIDNIGHTBLUE', relief='raise')
    bottomFrame.pack(side = BOTTOM)
    #============================================================================================
    ESLabel = Label(TopFrame, image=ESPhoto, bg='white')
    ESLabel.place(x=0,y=0)

    BVLabel = Label(bottomFrame, image=BVPhoto, bg='MIDNIGHTBLUE')
    BVLabel.place(x=210,y=-15)

    RobLabel = Label(bottomFrame, image=RobPhoto, bg='MIDNIGHTBLUE')
    RobLabel.place(x=400, y=50)



    #=======================Usuario and Senha and Cargo============================================

    UsuarioLabel = Label(bottomFrame, text='Usuario:', font=('Century Gothic', 20), bg='MIDNIGHTBLUE', fg='white')
    UsuarioLabel.place(x=110,y=110)
    Usuario = StringVar()
    UsuarioEntry = ttk.Entry(bottomFrame,width=30,textvariable=Usuario)
    UsuarioEntry.place(x=219, y=122,)

    SenhaLabel = Label(bottomFrame, text='Senha:', font=('Century Gothic', 20), bg='MIDNIGHTBLUE', fg='white')
    SenhaLabel.place(x=125,y=150)
    Senha = StringVar()
    SenhaEntry = ttk.Entry(bottomFrame, width=30, textvariable=Senha, show='•')
    SenhaEntry.place(x=219, y=161)

    CargoLabel = Label(bottomFrame, text='Cargo:', font=('Century Gothic',20), bg='MIDNIGHTBLUE', fg='white')
    CargoLabel.place(x=125,y=188)

    Decissao = StringVar()
    Prof = Radiobutton(bottomFrame, text='Professor', font=('Century Gothic',15), value='Professor',  variable=Decissao, bg='MIDNIGHTBLUE',fg='black')
    Prof.place(x=330, y=195)

    Alun = Radiobutton(bottomFrame, text='Aluno',font=('Century Gothic',15), value='Aluno', variable=Decissao, bg='MIDNIGHTBLUE',fg='black')
    Alun.place(x=225, y=195)
#==============================================================================================
    AmbienteS = Label(bottomFrame, text='Ambiente Seguro!✅', font=('Century Gothic',10), bg='MIDNIGHTBLUE', fg='white',relief=GROOVE)
    AmbienteS.place(x=424, y=250)

    Programador = Label(bottomFrame, text='Programador: Fabio Henrique✅', font=('Century Gothic',10), bg='MIDNIGHTBLUE',fg='white',relief=GROOVE)
    Programador.place(x=346,y=270)

    NenhumC = Label(bottomFrame, text='Nenhuma uma conta?', font=('Century Gothic',10), bg='MIDNIGHTBLUE', fg='Black')
    NenhumC.place(x=0, y=265)

#============================================================================================
    

    ExitButton = ttk.Button(bottomFrame, text='SAIR', width=10, command=lambda:Exit(Jan))
    ExitButton.place(x=220, y=240)

    Entrabutton = ttk.Button(bottomFrame, text='ENTRAR', width=15, command=lambda:Entrar(Usuario,Senha,Decissao))
    Entrabutton.place(x=299, y=240)

    Criarbutton = ttk.Button(bottomFrame, text='Criar!', width=5, command=lambda:Criar())
    Criarbutton.place(x=157, y=265)
    
    Jan.mainloop()

Menu()